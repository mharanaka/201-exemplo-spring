package br.com.itau.filmes.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.filmes.models.Ator;
import br.com.itau.filmes.services.AtorService;

@RestController
@RequestMapping("/ator")
public class AtorController {
	@Autowired
	AtorService atorService;
	
	@GetMapping
	public ArrayList<Ator> listarAtores(){
		return atorService.obterAtores();
	}
	
	@GetMapping("/{id}")
	public Ator obterAtor(@PathVariable int id) {
		return atorService.obterAtor(id);
	}
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public void inserirAtor(@RequestBody Ator ator) {
		atorService.inserirAtor(ator);
	}
}
