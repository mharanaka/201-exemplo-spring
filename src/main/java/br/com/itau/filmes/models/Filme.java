package br.com.itau.filmes.models;

import java.util.ArrayList;

public class Filme {
	private int id;
	private String titulo;
	private int ano;
	private ArrayList<Ator> atores;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	public ArrayList<Ator> getAtores() {
		return atores;
	}
	public void setAtores(ArrayList<Ator> atores) {
		this.atores = atores;
	}
}
